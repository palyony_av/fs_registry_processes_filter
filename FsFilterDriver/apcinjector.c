#include "apcinjector.h"
#include "injlib.h"
#include "config.h"

//////////////////////////////////////////////////////////////////////////
// APC initialization and deinitialization 
//////////////////////////////////////////////////////////////////////////

NTSTATUS InitializeApc(_In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath)
{
    NTSTATUS Status;
    INJ_SETTINGS Settings;

    Settings.DllPath[InjArchitectureX86].Length = sizeof(INJ_DLL_X86_FULL_NAME) - sizeof((INJ_DLL_X86_FULL_NAME)[0]);
    Settings.DllPath[InjArchitectureX86].MaximumLength = Settings.DllPath[InjArchitectureX86].Length;
    Settings.DllPath[InjArchitectureX86].Buffer = INJ_DLL_X86_FULL_NAME;
    InjDbgPrint("[injdrv]: DLL path (x86):   '%wZ'\n", Settings.DllPath[InjArchitectureX86]);

    Settings.DllPath[InjArchitectureX64].Length = sizeof(INJ_DLL_X64_FULL_NAME) - sizeof((INJ_DLL_X64_FULL_NAME)[0]);
    Settings.DllPath[InjArchitectureX64].MaximumLength = Settings.DllPath[InjArchitectureX64].Length;
    Settings.DllPath[InjArchitectureX64].Buffer = INJ_DLL_X64_FULL_NAME;
    InjDbgPrint("[injdrv]: DLL path (x64):   '%wZ'\n", Settings.DllPath[InjArchitectureX64]);

#if defined (_M_IX86)
    Settings.Method = InjMethodThunk;
#elif defined (_M_AMD64)
    Settings.Method = InjMethodThunkless;
#endif
    //
    // Initialize injection driver.
    //

    Status = InjInitialize(DriverObject, RegistryPath, &Settings);

    if (!NT_SUCCESS(Status))
    {
        InjDbgPrint("InjInitialize failed \n");
        return Status;
    }

    //
    // Install CreateProcess and LoadImage notification routines.
    //

    Status = PsSetCreateProcessNotifyRoutineEx(&InjCreateProcessNotifyRoutineEx, FALSE);

    if (!NT_SUCCESS(Status))
    {
        InjDbgPrint("PsSetCreateProcessNotifyRoutineEx failed \n");
        return Status;
    }

    Status = PsSetLoadImageNotifyRoutine(&InjLoadImageNotifyRoutine);

    if (!NT_SUCCESS(Status))
    {
        InjDbgPrint("PsSetLoadImageNotifyRoutine failed \n");
        PsSetCreateProcessNotifyRoutineEx(&InjCreateProcessNotifyRoutineEx, TRUE);
        return Status;
    }

    return STATUS_SUCCESS;
}

VOID DeinitializeApc()
{
    PsRemoveLoadImageNotifyRoutine(&InjLoadImageNotifyRoutine);
    PsSetCreateProcessNotifyRoutineEx(&InjCreateProcessNotifyRoutineEx, TRUE);

    InjDestroy();
}