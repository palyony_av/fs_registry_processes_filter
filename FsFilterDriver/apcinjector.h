#ifndef APCINJECTOR_H
#define APCINJECTOR_H

#include <ntddk.h>

NTSTATUS InitializeApc(_In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath);
VOID DeinitializeApc();

#endif // APCINJECTOR_H