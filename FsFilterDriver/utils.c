#include "utils.h"

#include <ntddk.h>

#define FLT_POOL_TAG     '0tfR'
#define FILE_PATH_OFFSET 8
#define BUFFER_LENGTH    1024

UNICODE_STRING ANTIVIRUS_FULL_NAME = RTL_CONSTANT_STRING(L"\\Device\\HarddiskVolume1\\Program Files\\PalyonyAV\\PalyonyAV.exe");

NTSTATUS
ZwQueryInformationProcess(
    IN HANDLE hProcessHandle,
    IN PROCESSINFOCLASS uProcessInformationClass,
    OUT PVOID pProcessInformation,
    IN ULONG uProcessInformationLength,
    OUT PULONG puReturnLength
);

NTSTATUS AddAnsiBufferToUnicodeStringArray(UNICODE_STRING* pArray, LONG elemIdx, char* buffer, LONG bufferLength)
{
    NTSTATUS status = -1;
    char* newBuffer = NULL;

    newBuffer = (char*)ExAllocatePoolWithTag(PagedPool, bufferLength + 1, FLT_POOL_TAG);

    if (newBuffer == NULL)
        return status;

    RtlZeroMemory(newBuffer, bufferLength + 1);
    RtlCopyMemory(newBuffer, buffer, bufferLength);

    ANSI_STRING newAnsiString;
    newAnsiString.Buffer = newBuffer;
    newAnsiString.Length = bufferLength;
    newAnsiString.MaximumLength = bufferLength;

    UNICODE_STRING newUnicodeString;
    newUnicodeString.Buffer = NULL;

    status = RtlAnsiStringToUnicodeString(&newUnicodeString, &newAnsiString, TRUE);

    if (NT_SUCCESS(status))
    {
        pArray[elemIdx].Buffer = newUnicodeString.Buffer;
        pArray[elemIdx].Length = newUnicodeString.Length;
        pArray[elemIdx].MaximumLength = newUnicodeString.MaximumLength;
    }

    ExFreePoolWithTag(newBuffer, FLT_POOL_TAG);

    return status;
}

NTSTATUS GetProcessFullImagePath(PEPROCESS Process, PUNICODE_STRING ImagePath)
{
    NTSTATUS status = STATUS_SUCCESS;
    ULONG returnedLength;
    HANDLE hProcess = NULL;

    // Get handle to target process
    status = ObOpenObjectByPointer(Process, OBJ_KERNEL_HANDLE, NULL, 0, NULL, KernelMode, &hProcess);

    if (NT_SUCCESS(status))
    {
        status = ZwQueryInformationProcess(hProcess, ProcessImageFileName, ImagePath->Buffer, ImagePath->MaximumLength, NULL);

        if (NT_SUCCESS(status))
        {
            ImagePath->Length = wcslen(ImagePath->Buffer) * 2;
        }
        else
        {
            DbgPrint("[-] ZwQueryInformationProcess failed: 0x%X", status);
        }

        ZwClose(hProcess);
    }
    else
    {
        DbgPrint("[-] ObOpenObjectByPointer failed: 0x%X", status);
    }

    return status;
}

BOOLEAN IsAntivirusProcess()
{
    NTSTATUS status;
    LONG result;

    UNICODE_STRING processName;
    processName.Buffer = (WCHAR*)ExAllocatePoolWithTag(PagedPool, sizeof(WCHAR) * BUFFER_LENGTH, FLT_POOL_TAG);
    processName.Length = 0;
    processName.MaximumLength = sizeof(WCHAR) * BUFFER_LENGTH;

    if (processName.Buffer)
    {
        status = GetProcessFullImagePath(PsGetCurrentProcess(), &processName);

        if (NT_SUCCESS(status))
        {
            DbgPrint("[*] Current process: %ws", processName.Buffer + FILE_PATH_OFFSET);
            //DbgPrint("[*] Antivirus      : %ws", ANTIVIRUS_FULL_NAME.Buffer);

            result = RtlCompareUnicodeStrings(
                processName.Buffer + FILE_PATH_OFFSET, processName.Length / 2 - FILE_PATH_OFFSET,
                ANTIVIRUS_FULL_NAME.Buffer, ANTIVIRUS_FULL_NAME.Length / 2,
                TRUE);

            if (!result)
            {
                ExFreePoolWithTag(processName.Buffer, FLT_POOL_TAG);
                return TRUE;
            }
        }

        ExFreePoolWithTag(processName.Buffer, FLT_POOL_TAG);
    }

    return FALSE;
}
