#include <fltKernel.h>
#include <dontuse.h>
#include <ntstrsafe.h>

#include "config.h"
#include "utils.h"
#include "apcinjector.h"

#define FLT_POOL_TAG '0tfR'

#define BUFFER_LENGTH    1024
#define FILE_PATH_OFFSET 8

UNICODE_STRING* g_FoldersBlockModify = NULL;
UNICODE_STRING* g_FilesBlockModify   = NULL;
UNICODE_STRING* g_KeysBlockModify    = NULL;

LONG g_FoldersBlockModifyCount = 0;
LONG g_FilesBlockModifyCount   = 0;
LONG g_KeysBlockModifyCount    = 0;

PFLT_PORT   g_Port         = NULL;
PFLT_PORT   g_ClientPort   = NULL;
PFLT_FILTER g_FilterHandle = NULL;

LARGE_INTEGER g_CmCookie = { 0 };

typedef struct _DRIVER_NOTIFICATION
{
    wchar_t EventBuffer[BUFFER_LENGTH];
    int     EventLength;
    wchar_t DateBuffer[BUFFER_LENGTH];
    wchar_t TimeBuffer[BUFFER_LENGTH];
} DRIVER_NOTIFICATION, PDRIVER_NOTIFICATION;

FLT_PREOP_CALLBACK_STATUS MiniPreCreate(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID* CompletionContext);
FLT_PREOP_CALLBACK_STATUS MiniPreWrite(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID* CompletionContext);
FLT_PREOP_CALLBACK_STATUS MiniPreSetInfo(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID* CompletionContext);

NTSTATUS MiniUnload(FLT_FILTER_UNLOAD_FLAGS Flags);

const FLT_OPERATION_REGISTRATION Callbacks[] = {
    { IRP_MJ_CREATE, 0, MiniPreCreate, NULL },
	{ IRP_MJ_WRITE, 0, MiniPreWrite, NULL },
    { IRP_MJ_SET_INFORMATION, 0, MiniPreSetInfo, NULL},
	{ IRP_MJ_OPERATION_END }
};

const FLT_REGISTRATION FilterRegistration = {
	sizeof(FLT_REGISTRATION),
	FLT_REGISTRATION_VERSION,
	0,
	NULL,
	Callbacks,
	MiniUnload,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

NTSTATUS SendLog(PUNICODE_STRING eventMessage)
{
    if (eventMessage == NULL)
    {
        DbgPrint("[-] eventMessage is null pointer");
        return -1;
    }

    DRIVER_NOTIFICATION LogMessage = { 0 };

    if (eventMessage->Length >= sizeof(LogMessage.EventBuffer))
    {
        DbgPrint("[-] Failed to send log to DLL because buffer is too big: %u bytes", eventMessage->Length);
        return -1;
    }

    LogMessage.EventLength = eventMessage->Length;
    RtlCopyMemory(LogMessage.EventBuffer, eventMessage->Buffer, LogMessage.EventLength);

    NTSTATUS status = STATUS_SUCCESS;

    // Take date and time fields using KeQuerySystemTime

    LARGE_INTEGER eventTime;
    TIME_FIELDS eventTimeFields;

    KeQuerySystemTime(&eventTime);
    ExSystemTimeToLocalTime(&eventTime, &eventTime);
    RtlTimeToTimeFields(&eventTime, &eventTimeFields);

    // Date filling

    UNICODE_STRING date = { 0 };

    date.Buffer = LogMessage.DateBuffer;
    date.MaximumLength = sizeof(LogMessage.DateBuffer);

    status = RtlUnicodeStringPrintf(&date, L"%d-%d-%d", eventTimeFields.Day, eventTimeFields.Month, eventTimeFields.Year);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] Failed to form (swprintf) date buffer for log");
        return status;
    }

    DbgPrint("[*] Date: %ws", date.Buffer);

    // Time filling

    UNICODE_STRING time = { 0 };

    time.Buffer = LogMessage.TimeBuffer;
    time.MaximumLength = sizeof(LogMessage.TimeBuffer);

    status = RtlUnicodeStringPrintf(&time, L"%d:%d:%d", eventTimeFields.Hour, eventTimeFields.Minute, eventTimeFields.Second);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] Failed to form (swprintf) time buffer for log");
        return status;
    }

    DbgPrint("[*] Time: %ws", time.Buffer);

    // Send data to dll via communication port with timeout

    // LARGE_INTEGER timeout;
    // timeout.QuadPart = 100;
    // status = FltSendMessage(g_FilterHandle, &g_ClientPort, &LogMessage, sizeof(LogMessage), NULL, NULL, &timeout);

    status = FltSendMessage(g_FilterHandle, &g_ClientPort, &LogMessage, sizeof(LogMessage), NULL, NULL, NULL);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] FltSendMessage has failed: %X", status);
        return status;
    }

    DbgPrint("[+] Log message has been sent to DLL");

    return STATUS_SUCCESS;
}

FLT_PREOP_CALLBACK_STATUS MiniPreCreate(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID* CompletionContext)
{
    // Prohibit file deletion
    
    if (Data->Iopb->Parameters.Create.Options & FILE_DELETE_ON_CLOSE)
    {
        return MiniPreWrite(Data, FltObjects, CompletionContext);
    }

    return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

BOOLEAN IsProtectedObject(PCUNICODE_STRING ObjectName)
{
    if (ObjectName == NULL)
        return FALSE;

    LONG i;
    LONG result;

    // Search directories matching

    for (i = 0; i < g_FoldersBlockModifyCount; i++)
    {
        if (ObjectName->Length < g_FoldersBlockModify[i].Length)
            continue;

        // compare (g_FoldersBlockModify[i].Length / 2) first bytes only
        // because we need to compare directory-part only 
        result = RtlCompareUnicodeStrings(
            ObjectName->Buffer, g_FoldersBlockModify[i].Length / 2,
            g_FoldersBlockModify[i].Buffer, g_FoldersBlockModify[i].Length / 2,
            TRUE
        );

        // handle slash
        if (result == 0 && 
            (ObjectName->Length == g_FoldersBlockModify[i].Length || 
                ObjectName->Buffer[g_FoldersBlockModify[i].Length / 2] == L'\\'))
        {
            return TRUE;
        }
    }

    // Search files matching

    for (i = 0; i < g_FilesBlockModifyCount; i++)
    {
        if (!RtlCompareUnicodeString(ObjectName, &g_FilesBlockModify[i], TRUE))
        {
            return TRUE;
        }
    }

    return FALSE;
}

BOOLEAN IsProtectedRegistryKey(PCUNICODE_STRING RegistryKey)
{
    if (RegistryKey == NULL)
        return FALSE;

    LONG i;
    LONG result;

    // Search keys matching

    for (i = 0; i < g_KeysBlockModifyCount; i++)
    {
        if (RegistryKey->Length < g_KeysBlockModify[i].Length)
            continue;

        // compare (g_KeysBlockModify[i].Length / 2) first bytes only
        // because we need to compare branch-part only 
        result = RtlCompareUnicodeStrings(
            RegistryKey->Buffer, g_KeysBlockModify[i].Length / 2,
            g_KeysBlockModify[i].Buffer, g_KeysBlockModify[i].Length / 2,
            TRUE
        );

        // handle slash
        if (result == 0 &&
            (RegistryKey->Length == g_KeysBlockModify[i].Length ||
                RegistryKey->Buffer[g_KeysBlockModify[i].Length / 2] == L'\\'))
        {
            return TRUE;
        }

    }

    return FALSE;
}

FLT_PREOP_CALLBACK_STATUS MiniPreWrite(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID* CompletionContext)
{
    NTSTATUS status;

    if (FltObjects == NULL || FltObjects->FileObject == NULL)
    {
        return FLT_PREOP_SUCCESS_NO_CALLBACK;
    }

    if (IsProtectedObject(&FltObjects->FileObject->FileName) && !IsAntivirusProcess())
    {
        DbgPrint("[*] File %ws modification attempt was prevented", FltObjects->FileObject->FileName.Buffer);

        Data->IoStatus.Status = STATUS_ACCESS_DENIED;
        Data->IoStatus.Information = 0;

        UNICODE_STRING eventMessage = { 0 };

        eventMessage.Buffer = (wchar_t*)ExAllocatePoolWithTag(PagedPool, BUFFER_LENGTH, FLT_POOL_TAG);
        eventMessage.MaximumLength = BUFFER_LENGTH;

        if (eventMessage.Buffer == NULL)
        {
            DbgPrint("[-] Failed to allocate memory for event message buffer");
            return FLT_PREOP_COMPLETE;
        }

        RtlZeroMemory(eventMessage.Buffer, BUFFER_LENGTH);

        status = RtlUnicodeStringPrintf(&eventMessage, L"File %ws modification attempt was prevented", FltObjects->FileObject->FileName.Buffer);

        if (!NT_SUCCESS(status))
        {
            DbgPrint("[-] Failed to form event message for log");
        }
        else
        {
            SendLog(&eventMessage);
        }

        if (eventMessage.Buffer)
            ExFreePoolWithTag(eventMessage.Buffer, FLT_POOL_TAG);

        return FLT_PREOP_COMPLETE;
    }

    return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

FLT_PREOP_CALLBACK_STATUS MiniPreSetInfo(PFLT_CALLBACK_DATA Data, PCFLT_RELATED_OBJECTS FltObjects, PVOID* CompletionContext)
{
    PVOID Buffer = Data->Iopb->Parameters.SetFileInformation.InfoBuffer;
    FILE_INFORMATION_CLASS fileInfoClass = Data->Iopb->Parameters.SetFileInformation.FileInformationClass;

    // Prohibit file deletion

    if (fileInfoClass == FileDispositionInformation && ((PFILE_DISPOSITION_INFORMATION)Buffer)->DeleteFile)
    {
        return MiniPreWrite(Data, FltObjects, CompletionContext);
    }

    if (fileInfoClass == FileDispositionInformationEx && ((PFILE_DISPOSITION_INFORMATION_EX)Buffer)->Flags & FILE_DISPOSITION_DELETE)
    {
        return MiniPreWrite(Data, FltObjects, CompletionContext);
    }

    // Prohibit file renaming

    if (fileInfoClass == FileRenameInformation)
    {
        return MiniPreWrite(Data, FltObjects, CompletionContext);
    }

    return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

NTSTATUS MiniConnect(PFLT_PORT ClientPort, PVOID ServerPortCookie, PVOID Context, ULONG Size, PVOID ConnectionCookie)
{
    g_ClientPort = ClientPort;
    DbgPrint("[+] Client has connected");

    return STATUS_SUCCESS;
}

VOID MiniDisconnect(PVOID ConnectionCookie)
{
    DbgPrint("[+] Client has disconnected");
    FltCloseClientPort(g_FilterHandle, &g_ClientPort);
}

void FreeFoldersBlockModifyArray()
{
    LONG i;

    for (i = 0; i < g_FoldersBlockModifyCount; i++)
        ExFreePoolWithTag(g_FoldersBlockModify[i].Buffer, FLT_POOL_TAG);

    if (g_FoldersBlockModify)
    {
        ExFreePoolWithTag(g_FoldersBlockModify, FLT_POOL_TAG);
        g_FoldersBlockModify = NULL;
    }

    g_FoldersBlockModifyCount = 0;
}

void FreeFilesBlockModifyArray()
{
    LONG i;

    for (i = 0; i < g_FilesBlockModifyCount; i++)
        ExFreePoolWithTag(g_FilesBlockModify[i].Buffer, FLT_POOL_TAG);

    if (g_FilesBlockModify)
    {
        ExFreePoolWithTag(g_FilesBlockModify, FLT_POOL_TAG);
        g_FilesBlockModify = NULL;
    }

    g_FilesBlockModifyCount = 0;
}

void FreeRegKeysBlockModifyArray()
{
    LONG i;

    for (i = 0; i < g_KeysBlockModifyCount; i++)
        ExFreePoolWithTag(g_KeysBlockModify[i].Buffer, FLT_POOL_TAG);

    if (g_KeysBlockModify)
    {
        ExFreePoolWithTag(g_KeysBlockModify, FLT_POOL_TAG);
        g_KeysBlockModify = NULL;
    }

    g_KeysBlockModifyCount = 0;
}

NTSTATUS MiniMessageNotifier(PVOID PortCookie, PVOID InputBuffer, ULONG InputBufferLength, PVOID OutputBuffer, ULONG OutputBufferLength, PULONG RetLength)
{
    DbgPrint("[+] Data has been received from client. Length: %d", InputBufferLength);
    DbgPrint("Data: %s", InputBuffer);

    // file string comes like "F_\Users\1\Desktop\folder\subfolder\file.txt\n"
    // dir string comes like  "D_\Users\1\Desktop\folder\subfolder\n"
    // reg string comes like  "R_\REGISTRY\MACHINE\SOFTWARE\PalyonyAV\n"

    LONG filesIdx = 0;
    LONG dirsIdx = 0;
    LONG keysIdx = 0;

    LONG i = 0, j = 0;
    char pathType;
    char* charInputBuffer = (char*)InputBuffer;

    // Free all previous allocated memory

    FreeFoldersBlockModifyArray();
    FreeFilesBlockModifyArray();
    FreeRegKeysBlockModifyArray();

    // Count files, directories and reg keys

    for (i = 0; i < InputBufferLength; i = j + 1)
    {
        pathType = charInputBuffer[i];

        for (j = i; charInputBuffer[j] != '\n'; j++)
            ;

        if (pathType == 'D')
            dirsIdx++;

        if (pathType == 'F')
            filesIdx++;

        if (pathType == 'R')
            keysIdx++;
    }

    DbgPrint("[*] Dir count  : %d", dirsIdx);
    DbgPrint("[*] File count : %d", filesIdx);
    DbgPrint("[*] Key count  : %d", keysIdx);

    g_FoldersBlockModifyCount = dirsIdx;
    g_FilesBlockModifyCount   = filesIdx;
    g_KeysBlockModifyCount    = keysIdx;

    dirsIdx = filesIdx = keysIdx = 0;

    // Memory allocation for folders global buffer

    if (g_FoldersBlockModifyCount)
    {
        g_FoldersBlockModify = ExAllocatePoolWithTag(PagedPool, g_FoldersBlockModifyCount * sizeof(UNICODE_STRING), FLT_POOL_TAG);

        if (g_FoldersBlockModify == NULL)
        {
            DbgPrint("[*] g_FoldersBlockModify pointer is NULL. Return ...");
            return -1;
        }

        RtlZeroMemory(g_FoldersBlockModify, g_FoldersBlockModifyCount * sizeof(UNICODE_STRING));
    }

    // Memory allocation for files global buffer

    if (g_FilesBlockModifyCount)
    {
        g_FilesBlockModify = ExAllocatePoolWithTag(PagedPool, g_FilesBlockModifyCount * sizeof(UNICODE_STRING), FLT_POOL_TAG);

        if (g_FilesBlockModify == NULL)
        {
            DbgPrint("[*] g_FilesBlockModify pointer is NULL. Return ...");
            return -1;
        }

        RtlZeroMemory(g_FilesBlockModify, g_FilesBlockModifyCount * sizeof(UNICODE_STRING));
    }

    // Memory allocation for registry keys global buffer

    if (g_KeysBlockModifyCount)
    {
        g_KeysBlockModify = ExAllocatePoolWithTag(PagedPool, g_KeysBlockModifyCount * sizeof(UNICODE_STRING), FLT_POOL_TAG);

        if (g_KeysBlockModify == NULL)
        {
            DbgPrint("[*] g_KeysBlockModify pointer is NULL. Return ...");
            return -1;
        }

        RtlZeroMemory(g_KeysBlockModify, g_KeysBlockModifyCount * sizeof(UNICODE_STRING));
    }

    // Fill global buffers for folders, files and reg keys

    for (i = 0; i < InputBufferLength; i = j + 1)
    {
        pathType = charInputBuffer[i];

        // Miss the first two bytes (D_ or F_ or R_)
        char* beginEntry = charInputBuffer + i + 2;

        for (j = i; charInputBuffer[j] != '\n'; j++)
            ;

        if (pathType == 'D')
        {
            AddAnsiBufferToUnicodeStringArray(g_FoldersBlockModify, dirsIdx, beginEntry, j - i - 2);
            dirsIdx++;
        }

        if (pathType == 'F')
        {
            AddAnsiBufferToUnicodeStringArray(g_FilesBlockModify, filesIdx, beginEntry, j - i - 2);
            filesIdx++;
        }

        if (pathType == 'R')
        {
            AddAnsiBufferToUnicodeStringArray(g_KeysBlockModify, keysIdx, beginEntry, j - i - 2);
            keysIdx++;
        }
    }

    return STATUS_SUCCESS;
}

NTSTATUS RfRegistryCallback(PVOID CallbackContext, PVOID Argument1, PVOID Argument2)
{
    if (Argument2 == NULL)
        return STATUS_SUCCESS;

    NTSTATUS status = STATUS_SUCCESS;
    PVOID object = NULL;

    REG_NOTIFY_CLASS Operation = (REG_NOTIFY_CLASS)(ULONG_PTR)Argument1;

    switch (Operation)
    {
    case RegNtPreDeleteKey:
        object = ((PREG_DELETE_KEY_INFORMATION)Argument2)->Object;
        break;
    case RegNtPreDeleteValueKey:
        object = ((PREG_DELETE_VALUE_KEY_INFORMATION)Argument2)->Object;
        break;
    case RegNtPreSetValueKey:
        object = ((PREG_SET_VALUE_KEY_INFORMATION)Argument2)->Object;
        break;
    case RegNtPreSetInformationKey:
        object = ((PREG_SET_INFORMATION_KEY_INFORMATION)Argument2)->Object;
        break;
    case RegNtPreRenameKey:
        object = ((PREG_RENAME_KEY_INFORMATION)Argument2)->Object; 
        break;
    default:
        return STATUS_SUCCESS;
    }
 
    PCUNICODE_STRING pRootObjectName = NULL;
    status = CmCallbackGetKeyObjectID(&g_CmCookie, object, NULL, &pRootObjectName);

    if (NT_SUCCESS(status) && pRootObjectName)
    {
        if (IsProtectedRegistryKey(pRootObjectName) && !IsAntivirusProcess())
        {
            DbgPrint("[*] Key %ws modification attempt was prevented", pRootObjectName->Buffer);

            UNICODE_STRING eventMessage = { 0 };

            eventMessage.Buffer = (wchar_t*)ExAllocatePoolWithTag(PagedPool, BUFFER_LENGTH, FLT_POOL_TAG);
            eventMessage.MaximumLength = BUFFER_LENGTH;

            if (eventMessage.Buffer == NULL)
            {
                DbgPrint("[-] Failed to allocate memory for event message buffer");
                return STATUS_ACCESS_DENIED;
            }

            RtlZeroMemory(eventMessage.Buffer, BUFFER_LENGTH);

            status = RtlUnicodeStringPrintf(&eventMessage, L"Key %ws opening attempt was prevented", pRootObjectName->Buffer);

            if (!NT_SUCCESS(status))
            {
                DbgPrint("[-] Failed to form event message for log");
            }
            else
            {
                SendLog(&eventMessage);
            }

            if (eventMessage.Buffer)
                ExFreePoolWithTag(eventMessage.Buffer, FLT_POOL_TAG);

            return STATUS_ACCESS_DENIED;
        }
    }

    return STATUS_SUCCESS;
}

NTSTATUS MiniUnload(FLT_FILTER_UNLOAD_FLAGS Flags)
{
	DbgPrint("[*] Driver unload");

    DeinitializeApc();
    CmUnRegisterCallback(g_CmCookie);
    FltCloseCommunicationPort(g_Port);
	FltUnregisterFilter(g_FilterHandle);

	return STATUS_SUCCESS;
}

NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS status;
    PSECURITY_DESCRIPTOR sd;
    OBJECT_ATTRIBUTES oa = { 0 };
    UNICODE_STRING name = COMM_PORT_NAME_REG_DATA;
    UNICODE_STRING AltitudeString = RTL_CONSTANT_STRING(L"360000");

    // Initialize APC

    status = InitializeApc(DriverObject, RegistryPath);
    
    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] Failed to initialize APC: 0x%X", status);
        return status;
    }

    // Register callback with the system for registry filtering

    status = CmRegisterCallbackEx(RfRegistryCallback, &AltitudeString, DriverObject, NULL, &g_CmCookie, NULL);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] CmRegisterCallbackEx has failed: %X", status);
        DeinitializeApc();
        return status;
    }

    // Register file system filter

	status = FltRegisterFilter(DriverObject, &FilterRegistration, &g_FilterHandle);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] FltRegisterFilter has failed: %X", status);
        DeinitializeApc();
        CmUnRegisterCallback(g_CmCookie);
        return status;
    }

    // Create Communication Port

    status = FltBuildDefaultSecurityDescriptor(&sd, FLT_PORT_ALL_ACCESS);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] FltBuildDefaultSecurityDescriptor has failed: %X", status);
        DeinitializeApc();
        CmUnRegisterCallback(g_CmCookie);
        FltUnregisterFilter(g_FilterHandle);
        return status;
    }

    InitializeObjectAttributes(&oa, &name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, sd);

    status = FltCreateCommunicationPort(g_FilterHandle, &g_Port, &oa, NULL, MiniConnect, MiniDisconnect, MiniMessageNotifier, 1);

    FltFreeSecurityDescriptor(sd);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] FltCreateCommunicationPort has failed: %X", status);
        DeinitializeApc();
        CmUnRegisterCallback(g_CmCookie);
        FltUnregisterFilter(g_FilterHandle);
        return status;
    }

    // Start filtering

    status = FltStartFiltering(g_FilterHandle);

    if (!NT_SUCCESS(status))
    {
        DbgPrint("[-] FltStartFiltering has failed: %X", status);
        DeinitializeApc();
        CmUnRegisterCallback(g_CmCookie);
        FltUnregisterFilter(g_FilterHandle);
        FltCloseCommunicationPort(g_Port);
        return status;
    }

    DbgPrint("[+] Driver has been successfully loaded");

	return STATUS_SUCCESS;
}