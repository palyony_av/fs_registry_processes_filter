#ifndef CONFIG_H
#define CONFIG_H

#define MODULE_ID 1

#define COMM_PORT_NAME_REG_DATA RTL_CONSTANT_STRING(L"\\PAV_CommPortFilterSubsystem")

#define INJ_DLL_X86_FULL_NAME L"..\\Program Files\\PalyonyAV\\injdllx86.dll"
#define INJ_DLL_X64_FULL_NAME L"..\\Program Files\\PalyonyAV\\injdllx64.dll"

#endif // CONFIG_H