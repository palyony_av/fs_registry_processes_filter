#ifndef UTILS_H
#define UTILS_H

#include <fltKernel.h>

NTSTATUS AddAnsiBufferToUnicodeStringArray(UNICODE_STRING* pArray, LONG elemIdx, char* buffer, LONG bufferLength);
NTSTATUS GetProcessFullImagePath(PEPROCESS Process, PUNICODE_STRING ImagePath);
BOOLEAN IsAntivirusProcess();

#endif // UTILS_H